const express = require('express');

const router = express.Router();

const Produit = require('../../model/Produit');

router.get('/', (req, res) => {
    Produit.find()
      .then((products) => {
        res.json(products);
      })
      .catch((error) => {
        console.error('Error retrieving products:', error);
        res.status(500).json({ error: 'Failed to retrieve products' });
      });
  });

  router.get('/:id', async (req, res) => {
    try {
      const id = req.params.id;
      const product = await Produit.findById(id);
  
      if (!product) {
        throw new ProductNotFoundException(`Le produit correspondant à l'id ${id} n'existe pas`);
      }
  
      res.json(product);
    } catch (error) {
      console.error('Error retrieving product:', error);
      res.status(500).json({ error: 'Failed to retrieve product' });
    }
  });

  
  module.exports = router ;