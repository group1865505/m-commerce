const mongoose = require ("mongoose");
const Schema = mongoose.Schema ;

const ProduitSchema = new Schema({
      titre: {
        type: String,
        required: true
      },
      description: {
        type: String,
        required: true
      },
      image: {
        type: String,
        required: true
      },
      prix:{
        type: Number,
        required : true
      }


});

module.exports = Produit = mongoose.model('produits', ProduitSchema );