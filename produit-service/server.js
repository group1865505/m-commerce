const express= require('express');
const bodyParser = require('body-parser');
const produits = require('./routes/api/produits');
require('dotenv').config();
const ConnectMongodb = require('./config/Db');

const app =express();
const cors = require('cors');

const corsOptions = {
  origin: '*' ,
  credentials: true,
  'allowedHeaders': ['sessionId', 'Content-type'],
  'exposedHeaders': ['sessionId'],
  'methods': 'GET, HEAD, PUT, PATCH, POST, DELETE',
  'preflightContinue': false
}
app.use(cors(corsOptions));
/*const cors = require('cors');
app.use(cors());*/
ConnectMongodb();

const port = process.env.PORT || 5002 ;
app.use(bodyParser.json());

app.use('/api/produits' , produits);
app.listen(port , ()=> console.log(`Le Microservice Produit est démaré sur le port ${port}`));

  const Produit = require('./model/Produit');
   const insertProducts = async () => {
   try {
      // Create an array of product objects
      const products = [
        { titre: 'HP Elitebook 840 G5 Laptop', description: 'Intel Core i7 1.80 GHz 16Gb Ram 512GB SSD Windows 10 Pro-64 (Renewed)',image:'/images/laptop1.jpeg', prix: 1466 },
        { titre: 'Acer Aspire 5 A515-56-347N Slim Laptop', description: '15.6" Full HD IPS Display - 11th Gen Intel i3-1115G4 Dual Core Process…',image:'/images/laptop2.jpeg', prix: 299 },
        { titre: 'Apple 2020 MacBook Air Laptop M1 Chip', description: '13" Retina Display, 8GB RAM, 256GB SSD Storage, Backlit Keyboard, Face…',image:'/images/laptop3.jpeg', prix: 704 },
        { titre: 'Dell 2022 Newest Inspiron 15 Laptop', description: '15.6" HD Display, Intel Celeron N4020 Processor, 16GB DDR4 RAM, 1TB PC…',image:'/images/laptop4.jpeg', prix: 389 },
        { titre: 'HP ENVY x360 Convertible 15-inch Laptop', description: 'AMD Ryzen 7 5825U processor, AMD Radeon Graphics, 8 GB RAM, 512 GB SSD…',image:'/images/laptop5.jpeg', prix:669 },
        { titre: 'HP 14 Laptop, AMD Ryzen 5 5500U', description: '8 GB RAM, 256 GB SSD Storage, 14-inch Full HD Display, Windows 11 Home…',image:'/images/laptop5.jpeg', prix:400 }
      ];
  
      // Insert the products into the database
      await Produit.insertMany(products);
  
      console.log('Products inserted successfully');
      
    } catch (error) {
      console.error('Error inserting products:', error);
    }
  };
insertProducts();



